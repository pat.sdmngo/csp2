const mongoose = require('mongoose');
const Product = require('./productModel');
const User = require('./userModel');


const cartSchema = new mongoose.Schema({
	status: {
		type: String,
		default: 'Pending'
	},
	user: {
		type: mongoose.Schema.Types.ObjectId,
		ref: 'User'
	},
	products: [{
		type: mongoose.Schema.Types.ObjectId,
		ref: 'Product'
	}]
});

module.exports = mongoose.model('Cart', cartSchema);
