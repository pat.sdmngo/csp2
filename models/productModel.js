const mongoose = require('mongoose');

const productSchema = new mongoose.Schema({
	name: {
		type: String,
		required: [true, 'Product name is required']
	},
	description: {
		productType: {
			type: String,
			required: true
		},
		color: {
			type: String,
			required: true
		},
		size: {
			type: String,
			required: true
		},
		quantity: {
			type: Number,
			default: 1
		}
	},
	collectionName: {
		type: String,
		default: 'General Collection'
	},
	price: {
		type: Number,
		required: [true, 'Product price is required']
	},
	isActive: {
		type: Boolean,
		default: true
	},
	createdOn: {
		type: Date,
		default: new Date()
	}
});

module.exports = mongoose.model('Product', productSchema);
