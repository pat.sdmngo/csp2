const mongoose = require('mongoose');
const User = require('./userModel');
const Cart = require('./cartModel')


const orderSchema = new mongoose.Schema({
	purchasedOn: {
		type: Date,
		default: new Date()
	},
	orderedBy: {
		type: mongoose.Schema.Types.ObjectId,
		ref: 'User'
	},
	shippingAddress: {
		type: String,
		required: [true, 'Shipping address is required']
	},
	cart: {
		type: Array
	},
	totalAmount: {
		type: Number,
		required: [true, 'Product total is required']
	}
});

module.exports = mongoose.model('Order', orderSchema);
