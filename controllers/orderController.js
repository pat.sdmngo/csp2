const User = require('../models/userModel');
const bcrypt = require('bcrypt');
const auth = require('../auth');


/* ----- GENERAL ----- */

// function for user registration and creation of account
module.exports.userRegistration = (req, res) => {
	User.findOne({email: req.body.email})
	.then(result => {
		if (result !== null) {
			res.send('Account already exists!')
		} else {
			let firstName = req.body.firstName;
			let lastName = req.body.lastName;
			let email = req.body.email;
			let password = req.body.password;
			let mobileNo = req.body.mobileNo;

			if ((firstName != '' && lastName != '' && email != '' && password != '' && mobileNo != '') &&
				(firstName != null && lastName != null && email != null && password != null && mobileNo != null) &&
				(firstName != undefined && lastName != undefined && email != undefined && password != undefined && mobileNo != undefined)) {

				const hashedPassword = bcrypt.hashSync(req.body.password, 10);
				let newUser = new User({
					firstName: firstName,
					lastName: lastName,
					email: email,
					password: hashedPassword,
					mobileNo: mobileNo
				});

				newUser.save()
				.then(user => res.send(user))
				.catch(err => res.send(err))
			} else {
				res.send('All fields are required!')
			}
		}
	})
	.catch(err => (res.send(err)))
};


// function for user login
module.exports.userLogin = (req, res) => {
	let email = req. body.email;
	let password = req.body.password;

	User.findOne({email: email})
	.then(foundUser => {
		if (foundUser == null || foundUser == undefined) {
			res.send('User not found!')
		} else {
			const isPasswordCorrect = bcrypt.compareSync(password, foundUser.password);
			if (isPasswordCorrect) {
				res.send({accessToken: auth.createAccessToken(foundUser)});
			} else {
				res.send('Incorrect email or password!')
			}
		}
	}).catch(err => res.send(err));
};


// function for retrieving user details
module.exports.getUserProfile = (req, res) => {
	let userId = req.user.id;

	User.findOne({_id: req.user.id})
	.then(foundUser => {res.send({profile: foundUser})})
	.catch(err => res.send(err));
};


// function for updating user details
module.exports.updateUserProfile = (req, res) => {
	let userId = req.user.id;
	let firstName = req.body.firstName;
	let lastName = req.body.lastName;
	let mobileNo = req.body.mobileNo;


	let updates = {$set: req.body};

	let options = {new: true};

	if ( !firstName && !lastName && !mobileNo ) {
		res.send('You cannot update that field!')
	} else {
		User.findByIdAndUpdate(userId, updates, options)
		.then(result => res.send({updatedInfo: result}))
		.catch(err => res.send(err));
	}
};


/* ----- ADMIN ----- */

// function to get all users
module.exports.getAllUsers = (req, res) => {
	User.find({})
	.then(users => res.send(users))
	.catch(err => res.send({message: 'An error occurred.', details: err}));
}


// function for setting a user to admin
module.exports.makeAdmin = (req, res) => {
	let userId = req.user.id;
	let makeAdmin = req.body.userId;

	User.findOne({_id: makeAdmin})
	.then(result => {
		if (result.isAdmin === false) {
			result.isAdmin = true;
			result.save()
			.then(admin => {res.send({message: 'Admin successfully added', adminAdded: result})})
			.catch(err => {res.send(err)})
		} else {
			res.send({message: 'User already set as an admin!'})
		}
	})
	.catch(err => res.send({message: 'An error occurred.', details: err}));
};


/* ============================================================== */

/* ----- ADDITIONAL FEATURES ----- */

// function to allow a user to delete own account 
module.exports.deleteAccount = (req, res) => {
	let userId = req.user.id;

	User.findOne({_id: userId}).then(foundUser => {
		if (!req.body.confirm) {
			res.send('Phew, that was close! Account not deleted.')
		} else {
			User.findOneAndDelete({_id: userId})
			.then(result => {
				res.send('Aww, we\'re sad to see you go :( \n But you\'re welcome to come back anytime. \n Goodbye!')
			}).catch(err => res.send({message: 'An error occured', details: err}))
		}
	}).catch(err => res.send({message: 'An error occured', details: err}))
}











