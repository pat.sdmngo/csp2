const express = require('express');
const router = express.Router();

const orderController = require('../controllers/orderController');
const auth = require('../auth');


/* ----- CUSTOMER ----- */

// add orders to cart
router.post('/orders', auth.verify, auth.customerVerification, orderController.addToCart);

// view cart details
router.get('/orders/cart', auth.verify, auth.customerVerification, orderController.findCart);

// proceed to checkout
router.post('/orders/cart/checkout', auth.verify, auth.customerVerification, orderController.checkOutOrder);

// view order details
router.get('/order-details', auth.verify, auth.customerVerification, orderController.showOrders);


/* ----- ADMIN ----- */

// view all orders
router.get('/orders', auth.verify, auth.adminVerification, orderController.allOrders);



module.exports = router;